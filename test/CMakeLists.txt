file(GLOB_RECURSE test-lib-src CONFIGURE_DEPENDS lib/*.c lib/*.h)
add_library(test-lib ${test-lib-src})

target_include_directories(test-lib PUBLIC lib)
add_compile_definitions(_DEFAULT_SOURCE)

file(GLOB_RECURSE test-src CONFIGURE_DEPENDS tests/*.c)
foreach(src IN LISTS test-src)
	# tests/mytest.c -> test-mytest
	string(REPLACE "/" ";" name ${src})
	list(GET name -1 name)
	string(REGEX REPLACE "\.c$" "" name ${name})
	set(name test_${name})

	list(APPEND all-test-targets ${name}) 

	add_executable(${name} ${src})
	target_link_libraries(${name} PRIVATE test-lib memalloc)
	add_test(NAME ${name} COMMAND ${name})
endforeach()

add_custom_target(check
	COMMAND ${CMAKE_CTEST_COMMAND} --output-on-failure -C $<CONFIG> --output-junit ${PROJECT_SOURCE_DIR}/report.xml
	DEPENDS ${all-test-targets})

