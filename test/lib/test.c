#include "test.h"
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>

#define MAX_MAPPINGS_TRACKED 64
typedef struct {
	void* addr;
	size_t len;
} Mapping;
static Mapping mappings[MAX_MAPPINGS_TRACKED];
static int num_mappings;

void* mmap_hook (void* addr, size_t len, int prot, int flags, int fd, off_t off)
{
	assert((uintptr_t) addr % getpagesize() == 0);
	assert(len > 0);
	assert(len % getpagesize() == 0);
	assert(prot == (PROT_READ | PROT_WRITE));

	const int required_flags = MAP_ANONYMOUS | MAP_PRIVATE;
	assert((flags & required_flags) == required_flags);
	assert(!(flags & MAP_FIXED));

	assert(fd == -1);
	assert(off == 0);

	void* result = mmap(addr, len, prot, flags, fd, off);
	if (result == MAP_FAILED)
		return MAP_FAILED;

	{ // Update the info about an existing mapping if we track it...
		bool found = false;
		for (int i = 0; i < MAX_MAPPINGS_TRACKED; i++) {
			if (((char*) result + len) == mappings[i].addr) {
				mappings[i].addr = result;
				mappings[i].len += len;
				found = true;
				break;
			}
		}
		// ...or track it as a new one if we can
		if (!found && num_mappings < MAX_MAPPINGS_TRACKED) {
			mappings[num_mappings].addr = result;
			mappings[num_mappings].len = len;
			num_mappings++;
		}
	}

	return result;
}

int munmap_hook (void* addr, size_t len)
{
	assert(addr);
	assert((uintptr_t) addr % getpagesize() == 0);
	assert(len > 0);
	assert(len % getpagesize() == 0);

	int result = munmap(addr, len);
	if (result == -1)
		return -1;

	{ // See if this undoes a mapping that we tracked...
		int found = -1;
		for (int i = 0; i < MAX_MAPPINGS_TRACKED; i++) {
			if (addr == mappings[i].addr) {
				found = i;
				break;
			}
		}
		// ...forget about it if it is
		if (found != -1) {
			assert(len == mappings[found].len);
			mappings[found] = mappings[--num_mappings];
		}
	}
	return result;
}

void assert_mmap_balance (void)
{
	assert(num_mappings == 0);
}
