#ifndef LIB_TEST_H
#define LIB_TEST_H

#ifdef NDEBUG
#undef NDEBUG
#endif

#ifndef DEBUG
#define DEBUG
#endif

#include <assert.h>
#include <stdlib.h>
#include <sys/mman.h>

void* mmap_hook (void* addr, size_t len, int prot, int flags, int fd, off_t off);
int munmap_hook (void* addr, size_t len);

void assert_mmap_balance (void);

#endif /* LIB_TEST_H */
