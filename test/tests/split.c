#include "mem.h"
#include "mem_internals.h"
#include "test.h"
#include <stdint.h>
#include <unistd.h>

/*
 * Checking that we have the expected number of regions and taken/free blocks
 */

typedef struct {
	int regions;
	int free_blocks;
	int taken_blocks;
} Counters;

void inc_region (void* addr, size_t size, void* data)
{
	(void) addr;
	(void) size;
	((Counters*) data)->regions++;
}

void inc_block (void* mem, size_t cap, bool taken, void* data)
{
	(void) mem;
	(void) cap;
	Counters* c = (Counters*) data;
	if (taken)
		c->taken_blocks++;
	else
		c->free_blocks++;
}

void assert_quantities (int regions, int free_blocks, int taken_blocks)
{
	Counters counters = { 0 };
	heap_walk(inc_region, inc_block, &counters);

	assert(counters.regions == regions);
	assert(counters.free_blocks == free_blocks);
	assert(counters.taken_blocks == taken_blocks);
}

int main (void)
{
	heap_callback_mapping(mmap_hook, munmap_hook);
	heap_init(0);
	assert_quantities(1, 1, 0);

	void* p = malloc_(1960);
	assert_quantities(1, 1, 1);

	void* q = malloc_(1960);
	assert_quantities(1, 1, 2);

	// Now there should not be enough memory for two allocations in the free block,
	// so it should not get split
	void* r = malloc_(20);
	assert_quantities(1, 0, 3);
	free_(r);

	free_(p);
	free_(q);
	assert_quantities(1, 1, 0);

	heap_deinit();
	assert_mmap_balance();
}
