#include "mem.h"
#include "mem_internals.h"
#include "test.h"
#include <stdint.h>
#include <unistd.h>

/*
 * Make a few allocations, and use `heap_walk()` to check a few assumptions
 * on the structure of the heap:
 *
 * - each taken block is a real allocation made by us
 * - each region ends exactly where its last block ends
 */


/*
 * mmap wrapper that ignores the hint, inhibiting any region growth. Helps get more
 * regions for this test, also tests sane behavior in the cases when region growth fails
 */
static void* mmap_ignore_hint (void* addr, size_t len, int flags, int prot, int fd, off_t off)
{
	(void) addr;
	return mmap_hook(NULL, len, flags, prot, fd, off);
}


typedef struct {
	void* addr;
	size_t size;
} Allocation;

typedef struct {
	Allocation* allocations;
	int num_allocations;
	void* latest_block_end;
	void* latest_region_end;
} Heap_walk_data;

static void on_region (void* restrict region, size_t size, void* restrict data)
{
	assert((uintptr_t) region % getpagesize() == 0);
	assert(size % getpagesize() == 0);

	Heap_walk_data* hwd = (Heap_walk_data*) data;
	// Check that the previous region ended where its last block did
	if (hwd->latest_region_end)
		assert(hwd->latest_region_end == hwd->latest_block_end);
	hwd->latest_region_end = (char*) region + size;
}

static void on_block (void* restrict mem, size_t cap, bool taken, void* restrict data)
{
	assert((uintptr_t) mem % 16 == 0);
	assert(cap % 16 == 0);
	assert(cap >= 64);

	Heap_walk_data* hwd = (Heap_walk_data*) data;
	hwd->latest_block_end = (char*) mem + cap;

	if (taken) {
		// Make sure each taken block corresponds to an allocation that has been made
		int found = -1;
		for (int i = 0; i < hwd->num_allocations; i++) {
			if (hwd->allocations[i].addr == mem) {
				assert(found == -1);
				assert(cap >= hwd->allocations[i].size);
				found = i;
			}
		}
		assert(found != -1);
		// Forget this allocation, to check that only one block is reported per allocation
		hwd->allocations[found] = hwd->allocations[--hwd->num_allocations];
	}
}

int main (void)
{
	heap_callback_mapping(mmap_ignore_hint, munmap_hook);
	heap_init(0);

#define NUM_ALLOCATIONS 200
	Allocation allocations[NUM_ALLOCATIONS];

	for (int i = 0; i < NUM_ALLOCATIONS; i++) {
		allocations[i].size = 2000 + 3000*i;
		allocations[i].addr = malloc_(allocations[i].size);
	}

	Heap_walk_data hwd = {
		.allocations = allocations,
		.num_allocations = NUM_ALLOCATIONS,
		.latest_block_end = NULL,
		.latest_region_end = NULL,
	};
	heap_walk(on_region, on_block, &hwd);

	// Check that the last region ended where its last block did
	assert(hwd.latest_block_end);
	assert(hwd.latest_block_end == hwd.latest_region_end);

	// Check that all allocations have been reported
	assert(hwd.num_allocations == 0);

	// Check that free_(NULL) has sane behavior
	free_(NULL);

	heap_deinit();
	assert_mmap_balance();
}
