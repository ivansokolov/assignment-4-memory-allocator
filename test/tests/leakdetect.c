#include "mem.h"
#include "mem_internals.h"
#include "test.h"

/*
 * Make a few allocations, free only half of them, and ensure that all the
 * other ones are reported on heap deinitialization with HEAP_LEAK_REPORT_FINE
 */

#define NUM_ALLOCATIONS 200

static void on_leak (void* start, void* end, void* data)
{
	(void) end;
	void** leaks = (void**) data;
	for (int i = 0; i < NUM_ALLOCATIONS/2; i++) {
		if (leaks[i] == start)
			leaks[i] = NULL;
	}
}

int main (void)
{
	void* allocations[NUM_ALLOCATIONS];
	void* leaks[NUM_ALLOCATIONS/2];

	heap_callback_mapping(mmap_hook, munmap_hook);
	heap_callback_leak(on_leak, HEAP_LEAK_REPORT_FINE, leaks);
	heap_init(0);

	for (int i = 0; i < NUM_ALLOCATIONS; i++) {
		allocations[i] = malloc_(200 + 300 * i);
		assert(allocations[i]);
	}

	for (int i = 0; 2*i < NUM_ALLOCATIONS; i++) {
		free_(allocations[2*i + 1]);
		leaks[i] = allocations[2*i];
	}

	heap_deinit();
	assert_mmap_balance();

	for (int i = 0; i < NUM_ALLOCATIONS/2; i++)
		assert(!leaks[i]);
}

