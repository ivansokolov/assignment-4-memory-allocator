#include "util.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

NORETURN void LIKE_PRINTF(1) fatal (const char* fmt, ...)
{
	fputs("Fatal: ", stderr);
	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args); // NOLINT
	va_end(args);
	fputc('\n', stderr);
	exit(1);
}

void LIKE_PRINTF(1) info (const char* fmt, ...)
{
	fputs("Info: ", stderr);
	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args); // NOLINT
	va_end(args);
	fputc('\n', stderr);
	fflush(stderr);
}
