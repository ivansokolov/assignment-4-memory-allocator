#ifndef MEM_H
#define MEM_H

#include <stddef.h>

void* malloc_ (size_t query);
void free_ (void* mem);

void heap_init (size_t initial_capacity);
void heap_deinit (void);

#endif /* MEM_H */
