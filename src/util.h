#ifndef UTIL_H
#define UTIL_H

#include <stddef.h>

#define ALIGNOF(T) _Alignof(T)
#define ALIGNED(AMT) __attribute__((aligned(AMT)))
#define ALIGNED_AS(T) ALIGNED(ALIGNOF(T))

#define IS_ALIGNED(PTR, AMT) ((uintptr_t) (PTR) % (AMT) == 0)
#define IS_ALIGNED_AS(PTR, T) IS_ALIGNED(PTR, ALIGNOF(T))

#define UNUSED(X) ((void) (X))

#define PACKED __attribute__((packed))
#define NORETURN _Noreturn
#define LIKE_PRINTF(NUM_ARGS) __attribute__((format(printf, NUM_ARGS, (NUM_ARGS)+1)))

/* Similar to `inline` in C++, in principle allows to define symbols in headers */
#define HEADERDEF __attribute__((weak))

#define LIKELY(X) __builtin_expect(!!(X), 1)
#define UNLIKELY(X) __builtin_expect(!!(X), 0)
#define ASSUME(X) do { if (!(X)) __builtin_unreachable(); } while (0)

#define STATIC_ASSERT(...) _Static_assert(__VA_ARGS__);


HEADERDEF size_t sz_max (size_t x, size_t y) { return (x >= y) ? x : y; }
HEADERDEF size_t sz_min (size_t x, size_t y) { return (x >= y) ? y : x; }
HEADERDEF size_t sz_ceil (size_t x, size_t d) { return ((x + d - 1) / d) * d; }

NORETURN void LIKE_PRINTF(1) fatal (const char* fmt, ...);
void LIKE_PRINTF(1) info (const char* fmt, ...);

#endif /* UTIL_H */