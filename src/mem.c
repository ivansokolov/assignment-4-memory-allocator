#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

/*
 * ================================ THE WAY mem.c WORKS ================================
 *
 * The allocator works by mmap'ing _regions_ of memory. Within each region, there is
 * one or more _block_, each of which represents a served allocation or usable space
 * for a future allocation.
 *
 * Regions are organized in a singly-linked list. The first entry in the list is the
 * most recently-added region. Because the list is singly-linked, most non-local heap
 * modification can only be performed on the latest region.
 *
 * Blocks are organized in a doubly-linked list within a region. There is no linkage
 * between blocks from different regions. Blocks don't actually have "next" pointers
 * either: they store their capacity, and each next block comes directly after the last.
 * But they do store a "prev" pointer, albeit without some lower bits, which are flags.
 *
 * When free blocks are found for allocations, they are typically split to serve as small
 * a block as possible, unless the other block would end up too small. When a split
 * happens, the _upper_ block is served, to match the downward growth described below.
 *
 * When there isn't a suitable block in the heap for allocation, the latest region
 * attempts to grow _downwards_. Downwards, because the kernel tries to use high
 * addresses for anonymous mappings, and there will typically already be things mapped
 * above the first mapping made, so downward growth succeeds a lot more.
 *
 * Freeing allocations is done by marking the corresponding block as free and merging it
 * with neighboring free blocks. No two consecutive blocks are ever free.
 */

// ======================================= Blocks =======================================

#define PAGE_SIZE 4096
#define MIN_BLOCK_CAPACITY 64

typedef struct { size_t bytes; } Aligned_size;
typedef struct { size_t bytes; } Page_aligned_size;

static Aligned_size sz_align (size_t s);
static Aligned_size sz_assume_aligned (size_t s);
static Page_aligned_size sz_page_align (size_t s);
static Page_aligned_size sz_assume_page_aligned (size_t s);

typedef ALIGNED_AS(max_align_t) struct Block {
	bool last: 1;           // Is this the last block within its region
	bool taken: 1;          // Taken for an allocation or not
	uintptr_t prev: 62;     // Block*, but shifted down by 2 (possible due to alignment)
	Aligned_size capacity;  // Number of usable bytes (without any headers)
	ALIGNED_AS(max_align_t) char memory[];
} Block;
#define BLOCK_OVERHEAD offsetof(Block, memory)
STATIC_ASSERT(IS_ALIGNED_AS(BLOCK_OVERHEAD, max_align_t), "Bad block header layout");

/*
 * Fully set up a block's header.
 * This on its own may not bring the region into a consistent state, because it does not
 * touch the supposed next block even if `!last`.
 * Call `block_relink()` also
 */
static void block_setup
(Block* block, bool last, bool taken, Block* prev, Aligned_size capacity);

static Block* block_get_next (const Block* block);
static Block* block_get_prev (const Block* block);

/*
 * Merge a block with its next block.
 * Assumes that this operation is correct: there is a next block, neither is taken, etc.
 */
static void block_merge_with_next (Block* block);

/*
 * Links two adjacent blocks so that `block_get_prev` and `block_get_next`
 * return correct values in the future, respectively. NULL is allowed in either.
 * The blocks must already be set up correctly, apart from the prev/next linkage!
 */
static void block_relink (Block* a, Block* b);

// ============================== Internal functions fwd ==============================

static bool any_regions_exist (void);
static bool region_push (size_t initial_capacity);
static void region_pop (void);

// Attempt to find an existing block that is at least `query` big.
static Block* try_find_block (Aligned_size query);

/*
 * Attempt to expand the latest region to create a block that is at least `query` big.
 * Returns such a block, or NULL on failure.
 * If a block is returned, it is guaranteed to have at least `query` capacity
 */
static Block* try_expand_latest_region (Aligned_size query);

// =============================== Main public interface ================================

void heap_init (size_t initial_capacity)
{
	if (any_regions_exist())
		fatal("Heap: Tried to initialize twice");
	if (!region_push(initial_capacity))
		fatal("Heap: Failed to acquire at least %zu usable bytes", initial_capacity);
}

void heap_deinit (void)
{
	if (!any_regions_exist())
		fatal("Heap: Tried to deinitialize nonexistent heap");
	do {
		region_pop();
	} while (any_regions_exist());
}


void* malloc_ (size_t query)
{
	if (!any_regions_exist()) fatal("Heap: Tried to allocate from nonexistent heap");
	if (query == 0) return NULL;

	Aligned_size aligned_query = sz_align(query);

	// Maybe there is a good existing block...
	Block* block = try_find_block(aligned_query);
	if (block) goto success;

	// ...or we have to request an expansion
	block = try_expand_latest_region(aligned_query);
	if (block) goto success;

	return NULL;

success:;
	size_t leftover = block->capacity.bytes - aligned_query.bytes;
	if (leftover < MIN_BLOCK_CAPACITY + BLOCK_OVERHEAD) {
		// Not enough capacity to split, use the block as is
		block->taken = true;
		return block->memory;
	} else {
		// There is enough memory for two blocks: split block into two, use the upper one

		// Important to retreive these before pilfering the block!
		Block* old_prev = block_get_prev(block);
		Block* old_next = block_get_next(block);

		Block* const low_block = block;
		Aligned_size low_block_capacity = sz_assume_aligned(leftover - BLOCK_OVERHEAD);

		Block* hi_block = (Block*) (low_block->memory + low_block_capacity.bytes);

		// setup high block first, because it uses info from the old (low) block
		block_setup(hi_block, // high block is now...
				low_block->last,  // last iff the non-split block used to be the last
				true,             // taken, we are returning it
				low_block,        // next for the low block
				aligned_query);
		block_setup(low_block, // low block is now...
				false,             // not last, there is at least the high block
				false,             // free, since the non-split block used to be
				old_prev,          // next for whatever it used to be still
				low_block_capacity);

		block_relink(hi_block, old_next);
		return hi_block->memory;
	}
}

void free_ (void* mem)
{
	if (!any_regions_exist()) fatal("Heap: Tried to deallocate from nonexistent heap");
	if (!mem) return;
	if (!IS_ALIGNED(mem, ALIGNOF(max_align_t)))
		fatal("Heap: tried to free misaligned pointer, clearly not from this heap");

	Block* block = (Block*) ((char*) mem - BLOCK_OVERHEAD);
	if (!block->taken) {
		fatal("Heap: tried to free pointer that was not from an allocated block. "
		      "Possible double-free or heap corruption");
	}

	block->taken = false;

	Block* next = block_get_next(block);
	Block* prev = block_get_prev(block);

	if (next && !next->taken) block_merge_with_next(block);
	if (prev && !prev->taken) block_merge_with_next(prev);
}

// ==================================== Blocks impl ====================================

static Aligned_size sz_align (size_t s)
{
	return (Aligned_size){ sz_ceil(s, ALIGNOF(max_align_t)) };
}

static Aligned_size sz_assume_aligned (size_t s)
{
	ASSUME(IS_ALIGNED_AS(s, max_align_t));
	return (Aligned_size){ s };
}

static Page_aligned_size sz_page_align (size_t s)
{
	return (Page_aligned_size){ sz_ceil(s, PAGE_SIZE) };
}

static Page_aligned_size sz_assume_page_aligned (size_t s)
{
	ASSUME(IS_ALIGNED(s, PAGE_SIZE));
	return (Page_aligned_size){ s };
}

static Block* block_get_next (const Block* block)
{
	if (block->last) return NULL;
	return (Block*) (block->memory + block->capacity.bytes);
}

static Block* block_get_prev (const Block* block)
{
	// NOLINTNEXTLINE inhibits escape analysis, whatever
	return (Block*) ((uintptr_t) block->prev << 2);
}

static void block_setup
(Block* restrict block, bool last, bool taken, Block* restrict prev, Aligned_size capacity)
{
	block->last = last;
	block->taken = taken;
	// NOLINTNEXTLINE inhibits escape analysis, whatever
	block->prev = ((uintptr_t) prev >> 2);
	block->capacity = capacity;
}

static void block_relink (Block* restrict a, Block* restrict b)
{
	if (a) {
		if (b) {
			// There's no `next` pointer, the next block implicitly starts after one's capacity
			assert((char*) a->memory + a->capacity.bytes == (char*) b);
			// A block cannot claim to be the last one and have the next one
			assert(!a->last);
		} else {
			assert(a->last); // Only the last block can have no next block
		}
	}
	if (b)
		b->prev = ((uintptr_t) a >> 2);
}

static void block_merge_with_next (Block* block)
{
	assert(!block->taken);
	Block* next = block_get_next(block);
	assert(next && !next->taken);

	Block* next2 = block_get_next(next);

	block_setup(block,         // block is now...
			next->last,            // last iff next used to be
			false,                 // not taken (hopefully did not use to be either)
			block_get_prev(block), // next to whatever it used to be
			// ...and its capacity spans both blocks now
			sz_assume_aligned(block->capacity.bytes + next->capacity.bytes + BLOCK_OVERHEAD));

	block_relink(block, next2);
}

// ===================================== Callbacks =====================================

static struct {
	Mmap_callback map;
	Munmap_callback unmap;

	Leak_callback leak;
	Leak_report_policy leak_policy;
	void* leak_user_data;
} callbacks;

static void* do_mmap (void* hint, Page_aligned_size size)
{
	return (callbacks.map ? callbacks.map : mmap)
		(hint, size.bytes, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
}

static int do_munmap (void* addr, Page_aligned_size size)
{
	return (callbacks.unmap ? callbacks.unmap : munmap)(addr, size.bytes);
}

void heap_callback_mapping (Mmap_callback map, Munmap_callback unmap)
{
	if (any_regions_exist())
		fatal("Heap: tried to change memory mapping functions while a heap already exists");
	callbacks.map = map;
	callbacks.unmap = unmap;
}

void heap_callback_leak (Leak_callback f, Leak_report_policy policy, void* user_data)
{
	callbacks.leak = f;
	callbacks.leak_policy = policy;
	callbacks.leak_user_data = user_data;
}

#pragma GCC poison mmap munmap // Use do_mmap, do_munmap instead

// ====================================== Regions ======================================

typedef ALIGNED(PAGE_SIZE) struct Region {
	Page_aligned_size mapped_size;
	struct Region* next;
	Block first_block[]; // Not an array! Only the first block in the linked list here
} Region;
#define REGION_OVERHEAD offsetof(Region, first_block)
STATIC_ASSERT(IS_ALIGNED_AS(REGION_OVERHEAD, max_align_t), "Bad region header layout");

static void region_relink (Region* a, Region* b) { a->next = b; }
static Region* region_get_next (const Region* region) { return region->next; }
#pragma GCC poison region_get_prev  // Cannot be done, this is not stored

static Region* region_full_setup (Region* region, Page_aligned_size size, Region* next);


static Region* latest_region;

static bool any_regions_exist (void)
{
	return !!latest_region;
}

static void region_setup_sole_block (Region* region)
{
	block_setup(region->first_block,
			true,  // sole block is last in its region
			false, // fresh block is not taken
			NULL,  // sole block has no previous blocks
			sz_assume_aligned(region->mapped_size.bytes - BLOCK_OVERHEAD - REGION_OVERHEAD));
}

static bool region_push (size_t initial_capacity)
{
	Page_aligned_size size
		= sz_page_align(initial_capacity + BLOCK_OVERHEAD + REGION_OVERHEAD);

	void* addr = do_mmap(NULL, size);
	if (addr == MAP_FAILED) return false;

	Region* new_region = addr;

	new_region->mapped_size = size;
	region_relink(new_region, latest_region);
	region_setup_sole_block(new_region);

	latest_region = new_region;
	return true;
}

static void region_pop (void)
{
	ASSUME(any_regions_exist());

	// Report any leaks if needed
	if (callbacks.leak
	&& (!latest_region->first_block->last || latest_region->first_block->taken)) {
		if (callbacks.leak_policy == HEAP_LEAK_REPORT_ROUGH) {
			callbacks.leak(latest_region->first_block->memory,
					(char*) latest_region + latest_region->mapped_size.bytes,
					callbacks.leak_user_data);
		} else if (callbacks.leak_policy == HEAP_LEAK_REPORT_FINE) {
			for (Block* b = latest_region->first_block; b; b = block_get_next(b)) {
				if (b->taken)
					callbacks.leak(b->memory, b->memory + b->capacity.bytes, callbacks.leak_user_data);
			}
		}
	}

	Region* next = region_get_next(latest_region);
	do_munmap(latest_region, latest_region->mapped_size);
	latest_region = next;
}


static Block* try_find_block (Aligned_size query)
{
	for (Region* reg = latest_region; reg; reg = region_get_next(reg)) {
		for (Block* block = reg->first_block; block; block = block_get_next(block)) {
			if (!block->taken && block->capacity.bytes >= query.bytes)
				return block;
		}
	}
	return NULL;
}

static Block* try_expand_latest_region (Aligned_size query)
{
	// It would be nice to reduce `query` by the amount of memory available from
	// the latest region's first block before rounding it up to pages, like so:
	// {
	//   Block* first = latest_region->first_block;
	//   Aligned_size tail_size = first->taken ? sz_assume_aligned(0) : first->capacity;
	//   query.bytes -= tail_size.bytes + REGION_OVERHEAD + BLOCK_OVERHEAD;
	// }
	// but that would underallocate in the case that a new region is created (when mmap
	// doesn't follow our hint), and we cannot tell in advance, so it is impossible

	Page_aligned_size map_size = sz_page_align(query.bytes);

	Region* desired = (Region*) ((char*) latest_region - map_size.bytes);
	Region* old_latest_region = latest_region;
	Region* mapped = do_mmap(desired, map_size);

	if (mapped == MAP_FAILED)
		return NULL;

	latest_region = mapped;

	if (mapped == desired) {
		// Successful expansion. Pilfer what used the be the old region header
		latest_region->mapped_size
			= sz_assume_page_aligned(old_latest_region->mapped_size.bytes + map_size.bytes);
		region_relink(latest_region, region_get_next(old_latest_region));

		block_setup(latest_region->first_block, // new-first-block is...
				false,  // ...not the last block (at least yet)
				false,  // ...definitely not taken
				NULL,   // ...definitely the first block
				// The space between new-first-block and new-first-block straddles both
				// `old_latest_region` header and new-first-block's header, but so far we
				// have to respect the latter, and reuse former in new-first-block's capacity...
				sz_assume_aligned(map_size.bytes - BLOCK_OVERHEAD));

		block_relink(latest_region->first_block, old_latest_region->first_block);

		// ...but if we can, reuse old-first-block too
		if (!old_latest_region->first_block->taken)
			block_merge_with_next(latest_region->first_block);
	} else {
		// Memory got allocated somewhere else instead: initialize a whole new region there
		latest_region->mapped_size = map_size;
		region_relink(latest_region, old_latest_region);
		region_setup_sole_block(latest_region);
	}

	return latest_region->first_block;
}

// ======================================= Debug =======================================

void heap_walk
(Heap_walk_callback_region cb_region, Heap_walk_callback_block cb_block, void* user_data)
{
	for (Region* reg = latest_region; reg; reg = region_get_next(reg)) {
		if (cb_region)
			cb_region(reg, reg->mapped_size.bytes, user_data);
		if (cb_block) {
			for (Block* block = reg->first_block; block; block = block_get_next(block))
				cb_block(block->memory, block->capacity.bytes, block->taken, user_data);
		}
	}
}

static void debug_print_block (void* mem, size_t size, bool taken, void* stream)
{
	fprintf((FILE*) stream,
			"   %s block, cap %zu, %p ... %p\n",
			taken ? "Taken" : "Free ",
			size, mem, (char*) mem + size);
}

static void debug_print_region (void* region, size_t size, void* stream)
{
	fprintf((FILE*) stream,
			"Region, size %zu, %p ... %p\n",
			size, region, (char*) region + size);
}

void heap_debug_text (FILE* stream)
{
	fputs("=========================== HEAP ==========================\n", stream);
	heap_walk(debug_print_region, debug_print_block, stream);
	fputs("========================= END HEAP ========================\n", stream);
}

