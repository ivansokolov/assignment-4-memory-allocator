#ifndef MEM_INTERNALS_H
#define MEM_INTERNALS_H

#include "util.h"
#include <stdbool.h>
#include <stdio.h>
#include <sys/mman.h>

/*
 * These callbacks must be API- and ABI-compatible with mmap()/munmap().
 * When set to NULL (the default), the heap defaults to using libc mmap()/munmap()
 * These can only be changed while the heap is uninitialized.
 */
typedef void* (*Mmap_callback) (void*, size_t, int, int, int, off_t);
typedef int (*Munmap_callback) (void*, size_t);
void heap_callback_mapping (Mmap_callback map, Munmap_callback unmap);


/*
 * Callback for each leaked allocation detected on initialization.
 * `policy` is ignored when the callback is being reset to NULL.
 */
typedef void (*Leak_callback) (void* start, void* end, void* user_data);
typedef enum {
	HEAP_LEAK_REPORT_ROUGH, // Crude reporting, reported ranges may contain many leaks
	HEAP_LEAK_REPORT_FINE,  // Fine reporting, one report per leaked allocation
} Leak_report_policy;
void heap_callback_leak (Leak_callback f, Leak_report_policy policy, void* user_data);


/*
 * Walk the heap and have callbacks called for each region and each block.
 * (See top of mem.c for an explanation about regions and blocks.)
 */
typedef void (*Heap_walk_callback_region)
	(void* region_address, size_t region_size, void* user_data);
typedef void (*Heap_walk_callback_block)
	(void* memory, size_t capacity, bool taken, void* user_data);
void heap_walk
(Heap_walk_callback_region cb_region, Heap_walk_callback_block cb_block, void* user_data);


/* Print the heap in a semi-human-readable way */
void heap_debug_text (FILE* stream);

#endif /* MEM_INTERNALS_H */
